import React from 'react';
import ReactDOM from 'react-dom'; 
import axios from 'axios';
import { data, InsertValue, Create, getAllCards, Delete, Edit} from './component/data'
import AddButton from './component/addButton';
import { Ready, InProgress, UnderReview, Completed } from './component/card';
import ModalForm from './component/modal';
let eventEmitter = require('events').EventEmitter;
require('/home/ajay/Desktop/card/src/component/style.css');

export const emitter = new eventEmitter(); 

class Wrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showHideModal : false,
      cards:[]
    };
  }

  componentWillMount() { 
    getAllCards();
  }
  
  componentDidMount() {
    emitter.on("updateTask", data => {
      const tasks = InsertValue(data);
      this.setState({cards : tasks});
    });

    emitter.on("create", task => {
      data.push(task);
      this.setState({cards : data});
    });

    emitter.on("delete", id => {
      let card;
      {data.map((task) => {
        if(task.id.toString() === id) {
          card = task;
        }
      })}
      data.splice(data.indexOf(card) , 1);
      this.setState({cards: data});
    });

    emitter.on("edit", responseData => {
      data.splice(data.indexOf(responseData) , 1, responseData);
      this.setState({cards: data});
    })
  }
  
  componentWillUnmount() {
    emitter.removeAllListener();
  }

  myClick = e => {
    if(e.target.id.includes('Modal')) {
      const prevState = this.state.showHideModal;
      if(prevState === false) {
        this.setState ({
            showHideModal : true
        });
      } else {
        this.setState ({
            showHideModal : false
        });
      }
    }
  }

  create = FormData => {
    Create(FormData);
  }

  deleteCard = (e) => {
    Delete(e);
  }

  edit = (FormData) => {
    Edit(FormData);
  }

  render() {
    return (
      <React.Fragment>
        <AddButton onClick={this.myClick}/>
        <div className="wrapper" id="wrapper">
          <div className="readylane" id="ready">
            <button className="ready">Ready</button>
            <Ready deleteCard={this.deleteCard} edit={this.edit}/>
          </div>
          <div className="inprogresslane" id="inprogress">
            <button className="inprogress">Inprogress</button>
            <InProgress deleteCard={this.deleteCard} edit={this.edit}/>
          </div>
          <div className="underreviewlane" id="review">
            <button className="underreview">UnderReview</button>
            <UnderReview deleteCard={this.deleteCard} edit={this.edit}/>
          </div>
          <div className="completedlane" id="completed">
            <button className="completed">Completed</button>
            <Completed deleteCard={this.deleteCard} edit={this.edit}/>
          </div>
        </div>
        <ModalForm showHideModal={this.state.showHideModal} onClick={this.myClick} create={this.create}/>
      </React.Fragment>
    )
  }
}
ReactDOM.render(
  <Wrapper/>, 
  document.getElementById("body"));
