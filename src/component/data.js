import React from 'react'
import { emitter } from '/home/ajay/Desktop/card/src/index'
import axios from 'axios'

export const data = []

export const InsertValue = (allData) => {
  {allData.map((datum) => {
    return data.push(datum) 
  })}
}

export const Create = FormData => {
  axios({
    method: 'post',
    url: 'http://192.168.8.7:3000/api/cards',
    data: {
    name: FormData.name,
    description: FormData.description,
    completedPercentage: FormData.completedPercentage,
    status: FormData.status
    }
  }).then( response => {
    emitter.emit("create", response.data);
  });
}

export const getAllCards = () => {
  axios({
    method:'get',
    url:'http://192.168.8.7:3000/api/cards',
    contentType : "application/json",
    dataType : "json",
  }).then( response => {
    emitter.emit("updateTask", response.data);
  });
}

export const Delete = e => {
  let id = e.target.id.split("delete")[1];
  axios({
    method:'DELETE',
    url:'http://192.168.8.7:3000/api/cards/' + id,
  }).then(response => {
    emitter.emit("delete", id);
  });
}

export const Edit = FormData => {
  axios({
    method: 'PATCH',
    url: 'http://192.168.8.7:3000/api/cards/' + FormData.id,
    data: {
    name: FormData.name,
    description: FormData.description,
    completedPercentage: FormData.completedPercentage,
    status: FormData.status
    }
  }).then( response => {
    emitter.emit("edit", response.data);
  });
}

