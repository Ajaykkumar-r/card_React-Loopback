import React from 'react'
import axios from 'axios';
require('./style.css');

class ModalForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:props.id, 
      name: props.name, 
      description:props.description, 
      completedPercentage:props.percentage, 
      status:props.status,
      showNameMessage: false,
      showDescriptionMessage : false,
      showPercentageMessage : false,
      button: props.button
    } 
    this.input = this.input.bind(this);
    this.validate = this.validate.bind(this);
  };

  input(e) {
    let result = e.target.value === 'clear' ? '' : e.target.value;
    if(e.target.id === 'name') {
        this.setState({name: result});
    } else if (e.target.id === 'description') {
        this.setState({description: result});
    } else if (e.target.id === 'percentage') {
        this.setState({completedPercentage: result});
    } else if (e.target.id === 'status') {
        this.setState({status: e.target.value});
    }
  }
  
  validate(e) {
    if(e.target.id === 'name' && e.target.value === '' || e.target.value.length > 20) {
      this.setState({showNameMessage: true});
    } else if(e.target.id === 'name' && e.target.value !== '' && e.target.value.length < 20) {
       this.setState({showNameMessage : false});
    }
    if(e.target.id === 'description' && e.target.value === '' || e.target.value.length > 160) {
      this.setState({showDescriptionMessage: true});
    } else if(e.target.id === 'description' && e.target.value !== '' && e.target.value.length < 160) {
      this.setState({showDescriptionMessage : false});
    }
    if(e.target.id === 'percentage' && e.target.value === '' || e.target.value > 100 || e.target.value < 0) {
      this.setState({showPercentageMessage: true});
    } else if(e.target.id === 'percentage' && e.target.value !== '' && e.target.value < 100 && e.target.value > 0) {
      this.setState({showPercentageMessage : false});
    } 
    if(this.state.name && this.state.description && this.state.completedPercentage) {
      this.setState({button:false});
    } else {
      this.setState({button:true});
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      id:nextProps.id, 
      name: nextProps.name, 
      description: nextProps.description, 
      completedPercentage: nextProps.percentage, 
      status: nextProps.status,
      button: nextProps.button
    })
  }

 

  clear = e => {
    console.log("clear");
    this.setState({ 
      name: null, 
      description: null, 
      completedPercentage: null, 
      status: "Ready"
    })
  }

  render() {
    return (
      <div className="modal fade in" id="myModal" role="dialog" style={{display: this.props.showHideModal ? 'block' : 'none'}}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <a className="right fa fa-close" id="closeModal" onClick={this.props.onClick} />
                <h2 className="modal-title">Card details</h2>
              </div>
              <div className="modal-body">
                <div className="center">
                  <label>Name</label> 
                  <span>
                      <input type="text" className="input right" id="name"  onInput={this.input} onChange= {this.validate} onBlur={this.validate} value={this.state.name}/>
                      <p className="right hidden message" id="nameErrorMessage" style={{display: this.state.showNameMessage ? 'block' : 'none'}}>Please fill the name(max:20Characters)</p>
                  </span>
                </div>
                <div className="center">
                  <label>Description</label> 
                  <span>
                      <input type="text" className="input right" id="description" onInput={this.input} onChange= {this.validate} onBlur={this.validate} value={this.state.description}/>
                      
                      <p className="right hidden message" id="descriptionErrorMessage" style={{display: this.state.showDescriptionMessage ? 'block' : 'none'}}>Please fill the description(max:160Characters)</p>
                  </span>
                </div>
                <div className="center">
                  <label>Completed percentage</label> 
                  <span>
                      <input type="number" className="input right" id="percentage" onInput={this.input} onChange= {this.validate} onBlur={this.validate} value={this.state.completedPercentage}/>
                    
                      <p className="right hidden message" id="percentageErrorMessage" style={{display: this.state.showPercentageMessage ? 'block' : 'none'}}>Please fill the percentage(max:100)</p>
                  </span>
                </div>
                <div className="center">
                  <label>Status</label>
                  <select className="right" id="status" onChange={this.input} value={this.state.status}>
                      <option value="Ready">Ready</option>
                      <option value="Inprogress">Inprogress</option>
                      <option value="Underreview">Under review</option>
                      <option value="Completed">Completed</option>
                  </select>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" disabled={this.state.button} className="save right" id="saveModal" onClick={(e) => {this.props.create(
                  { id:this.state.id,
                    name: this.state.name,
                    description: this.state.description,
                    completedPercentage: this.state.completedPercentage,
                    status: this.state.status});this.clear(e);
                    this.props.onClick(e);}} >Save</button>
            </div>
            </div>
          </div>
      </div>
    )
  }
}

ModalForm.defaultProps = { id:null,name:null, description:null, completedPercentage: null, status: "Ready" , button: true};

export default ModalForm