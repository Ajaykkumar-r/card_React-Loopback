import React, { Component } from 'react';
import { data } from './data';
import ModalForm from './modal';
require('./style.css');

export class Ready extends Component {
  render() {
    return (
      data.map((datum) => {
        return (datum.status === "Ready" ?
         <Card cardDetails = {datum} deleteCard={this.props.deleteCard} edit={this.props.edit}/> : "")
      })
    );
  }
}

export class InProgress extends Component {
  render() {
    return (
      data.map((datum) => {
       return (datum.status === "Inprogress" ? 
       <Card cardDetails = {datum} deleteCard={this.props.deleteCard} edit={this.props.edit}/> : "" )
      })
    )
  }
}

export class UnderReview extends Component {
  render() {
    return (
      data.map((datum) => {
        return (datum.status === "Underreview" ?
         <Card cardDetails = {datum} deleteCard={this.props.deleteCard} edit={this.props.edit}/> : "")
      })
    )
  }
}

export class Completed extends Component {
  render() {
    return (
      data.map((datum) => {
        return (datum.status === "Completed" ?
         <Card cardDetails = {datum} deleteCard={this.props.deleteCard} edit={this.props.edit}/> : "" )
      })
    )
  }
}

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editDeleteOption : false,
      showModal : false,
      id:null,
      name:null,
      descriptin:null,
      percentage:null,
      status:null,
    };
  }

  editDeleteHandleClick = (e) => {
    if(e.target.id.includes('option')) {
        let prevState = this.state.editDeleteOption;
        if(prevState === false) {
          this.setState ({
            editDeleteOption : true
          });
        } else {
          this.setState ({
            editDeleteOption : false
          });
        }
    }
  }

  editCard = (e) => {
    console.log("entered edit", e);
    data.map((datum) => {
      if(datum.id.toString() === e.target.id.split("edit")[1]) {
        this.setState({
          showModal: true,
          id: datum.id,
          name:datum.name,
          description:datum.description,
          percentage:datum.completedPercentage,
          status:datum.status,
          button:false
        });
      }
    })
  }

  myClick = (e) => {
    if(e.target.id.includes('Modal')) {
        let prevState = this.state.showModal;
        if(prevState === false) {
          this.setState ({
            showModal : true,
            editDeleteOption : true
          });
        } else {
          this.setState ({
            showModal : false,
            editDeleteOption : false
          });
        }
    }
  }
  render() {
    const {showModal, id, name, description, percentage, status, editDeleteOption, button} = this.state;
    const { cardDetails, deleteCard, edit } =this.props;
    let width = (cardDetails.completedPercentage * 2.4) + "px";
    return (
      <div className="card" id={`card${cardDetails.id}`} key={`card${cardDetails.id}`}>
        <div className="option">
          <i className="right fa fa-ellipsis-v" id={`option${cardDetails.id}`} 
              key= {`option${cardDetails.id}`} onClick={this.editDeleteHandleClick}></i>
          <a id={`edit${cardDetails.id}`} key={`edit${cardDetails.id}`} className="edit" 
              style={{display: editDeleteOption ? 'block' : 'none'}} onClick={this.editCard}>Edit</a>
          <a id={`delete${cardDetails.id}`} key={`delete${cardDetails.id}`} className="delete" 
              style={{display: editDeleteOption ? 'block' : 'none'}} onClick={deleteCard}>Delete</a>
        </div>
        {this.state.showModal &&
          <ModalForm showHideModal={showModal} onClick={this.myClick} create={edit}  button={button}
          id={id} name={name} description={description} 
          percentage={percentage} status={status}/>
        }
        <div className="displayname">
          <label>Name:</label>
          <span>
            <p id={`namevalue${cardDetails.id}`} 
                key={`namevalue${cardDetails.id}`} >{cardDetails.name}</p>
          </span>
        </div>
        <div className="displaydescription">
          <label>Description:</label>
          <span>
            <p id={`descriptionvalue${cardDetails.id}`} 
                key={`descriptionvalue${cardDetails.id}`} >{cardDetails.description}</p>
          </span>
        </div>
        <div className="displaypercentage">
          <a className="progress-bar" id={`percentagevalue${cardDetails.id}`} 
              key={`percentagevalue${cardDetails.id}`} style={width = {width}}>
              {cardDetails.completedPercentage}</a>
        </div>
      </div>
    )
  }
}
