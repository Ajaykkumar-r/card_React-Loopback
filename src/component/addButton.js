import React from 'react';
require('./style.css');

class AddButton extends React.Component {
    render() {
        return (
            <div className="top">
                <button className="fa fa-plus" id="addModal" onClick={this.props.onClick}>Add</button>
            </div>
        )
    }
}

export default AddButton